class Stack
  def initialize
    @items = []
  end

  def is_empty
    @items.count == 0
  end

  def push item
    @items.append item
  end

  def pop
    @items.pop
  end

  def peek
    @items[-1]
  end
end
