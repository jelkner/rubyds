def gcf n1, n2
  if n1 < n2
    n1, n2 = n2, n1
  end
  while n2 != 0
    t, n2 = n2, n1 % n2
    n1 = t
  end
  return n1
end

class Fraction
  attr_reader :n, :d

  def initialize n, d
    cf = gcf n, d
    @n = n/cf
    @d = d/cf
  end

  def to_s
    "#{@n}/#{@d}"
  end

  def * other
    Fraction.new self.n * other.n, self.d * other.d
  end

  def numerator 
    @n
  end

  def denominator
    @d
  end
end

f = Fraction.new 6,8
puts f

puts gcf 12, 30

f2 = Fraction.new 5,7

puts f * f2
