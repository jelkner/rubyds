it 'score is 10 for a strike - knocking down all ten pins' do
  game = BowlingGame.new
  game.strike

  assert_equal 10, game.score
end
